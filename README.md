## multi\_ar

This is scaffolded runtime directory for project dyyy, created by command multi\_ar --init ..

Purpose of this scaffold is to ease usage of this multi\_ar, by providing sample configuration and ready
bundle just ready for running. Default configuration is for SQLite3, but any databases supported
by Activerecord can be used (you may need to add them to Gemfile in order for more rare adapters to work).

You can run multi\_ar using bundler:

    bundle exec multi\_ar

multi\_ar is powered by ActiveRecord migration framework MultiAR-5.1.3. More information about MultiAR
can be found at [its home page](https://github.com/Smarre/multi_ar)

More information bundler can be found [at its homepage](http://bundler.io)
