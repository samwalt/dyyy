class CreateBlDrugImage < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_drug_image, {id: false} do |t|
      t.decimal :artiid, :precision=>22, :comment=>'商品id'
      t.string :path, :limit=>200, :comment=>'图片路径'
      t.string :type, :limit=>50, :comment=>'图片类型'
    end
  end
end
