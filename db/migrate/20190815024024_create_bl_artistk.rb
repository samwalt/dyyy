class CreateBlArtistk < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_artistk, {id: false} do |t|
      t.string :deptno, :limit=>200, :comment=>'门店id'
      t.decimal :artiid, :precision=>22, :comment=>'商品id'
      t.string :articode, :limit=>20, :comment=>'商品编码'
      t.string :artibarcode, :limit=>200, :comment=>'条形码'
      t.string :artiname, :limit=>200, :comment=>'商品通用名'
      t.string :artiabbr, :limit=>200, :comment=>'商品名'
      t.string :artispec, :limit=>200, :comment=>'规格'
      t.string :artiunit, :limit=>20, :comment=>'单位'
      t.string :manufactory, :limit=>200, :comment=>'生产厂家'
      t.decimal :retailprice, :precision=>16, :scale=>6, :comment=>'零售价'
      t.string :condition, :limit=>100, :comment=>'存储条件'
      t.string :yblx, :limit=>20, :comment=>'医保类型'
      t.string :ybsx, :limit=>20, :comment=>'医保属性'
      t.string :artinameadd, :limit=>200, :comment=>'医保商品通用名'
      t.string :artiabbradd, :limit=>200, :comment=>'医保商品名'
      t.string :artispecadd, :limit=>120, :comment=>'医保规格'
      t.string :artiunitadd, :limit=>20, :comment=>'医保单位'
      t.string :manufactoryadd, :limit=>200, :comment=>'医保厂家'
      t.decimal :ybarti, :precision=>22, :comment=>'是否医保药'
      t.string :ybcode, :limit=>20, :comment=>'医保编码'
      t.decimal :stkqty, :precision=>16, :scale=>6, :comment=>'库存数量'
      t.string :articlasscode, :limit=>100, :comment=>'商品分类'
      t.string :authorize, :limit=>80, :comment=>'批准文号'
    end
  end
end
