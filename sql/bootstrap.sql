drop table if exists hp_shelf;
create table hp_shelf (
    id bigint auto_increment,
    store_id bigint comment '门店id',
    device_id bigint comment '货架id',
    region varchar(50) comment '地区', 
    province varchar(50) comment '省份', 
    city varchar(50) comment '城市', 
    store_name varchar(50) comment '店铺名', 
    shelf_name varchar(50) comment '货架名',
    updated_at datetime,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists hp_item;
create table hp_item (
    id bigint auto_increment,
    store_id bigint comment '门店id',
    device_id bigint comment '货架id',
    cell_id bigint comment '商品标定的区域编号', 
    sku_id bigint comment '商品的sku id', 
    item_name varchar(50) comment '商品名称',
    updated_at datetime,
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists hp_shelf_conversion;
create table hp_shelf_conversion (
    store_id bigint comment '门店id',
    device_id bigint comment '货架id',
    pass_people smallint comment '经过货架的人数', 
    stay_people smallint comment '在货架前停留的人数', 
    interact_people smallint comment '与货架交互的人数',
    updated_at date
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists hp_item_conversion;
create table hp_item_conversion (
    store_id bigint comment '门店id',
    device_id bigint comment '货架id',
    cell_id bigint comment '商品标定的区域编号', 
    sku_id bigint comment '商品的sku id', 
    interact_people smallint comment '与货架交互的人数',
    interact_count smallint comment '与货架交互的次数',
    updated_at date
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists bl_drug;
create table bl_drug (
    id bigint auto_increment,
    artiid bigint comment 'ERP商品ID',
    manufactory varchar(200) comment 'ERP生产厂家',
    authorize varchar(80) comment 'ERP批注文号',
    articode varchar(20) comment 'ERP商品编码',
    drug_id bigint comment '药品id',
    drug_code varchar(20) comment '药品编号',
    artiname varchar(200) comment '药品名称',
    purpose varchar(500) comment '功能主治',
    usage_mode varchar(5000) comment '用法用量',
    contraindication varchar(2000) comment '禁忌症',
    retailprice decimal(16,6) comment '价格',
    yblx varchar(20) comment '医保类型',
    ybfl varchar(20) comment '医保分类',
    ybsx varchar(20) comment '医保属性',
    dope varchar(5) comment '是否含有兴奋剂',
    artilimit tinyint comment '销售限量',
    usage_limit varchar(100) comment '限定使用',
    prescription varchar(5) comment '处方标志',
    store_prescription varchar(5) comment '门店处方标志',
    articlasscode varchar(100) comment '类别',
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists hk_customer_detail;
create table hk_customer_detail (
    tenant_id varchar(64) comment '租户id',
    store_id varchar(32) comment '门店id',
    store_name varchar(64) comment '门店名称',
    device_serial varchar(64) comment '设备序列号',
    channel_no smallint comment '通道号',
    resource_name varchar(64) comment '资源名称',
    user_type tinyint comment '用户类型(0:全部1:会员 2:员工 3惯偷 4:回头客5:普通顾客)',
    face_token varchar(64) comment '人脸图token',
    face_url varchar(255) comment '人脸图url',
    standard_token varchar(64) comment '标准图token',
    standard_url varchar(255) comment '标准图url',
    bg_url varchar(255) comment '背景图url',
    similarity varchar(32) comment '相似度',
    sex tinyint,
    age tinyint,
    glasses tinyint comment '是否戴眼镜0:无眼镜 1:有眼镜',
    face_time bigint comment '人脸图时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
alter table hk_customer_detail add unique uniq_device_serial_face_time (device_serial, face_time);

drop table if exists hk_device;
create table hk_device (
    id bigint auto_increment,
    device_id varchar(32) comment '设备id',
    device_name varchar(32) comment '设备名称',
    device_model varchar(32) comment '设备型号',
    device_serial varchar(16) comment '设备序列号',
    channel_id varchar(32) comment '通道id',
    channel_name varchar(64) comment '通道名称',
    channel_no bigint comment '通道号',
    channel_status varchar(1) comment '状态 0：离线，1：在线',
    channel_pic_url varchar(255) comment '通道封面图片URL',
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

drop table if exists hk_customer_statistics;
create table hk_customer_statistics (
    id bigint auto_increment,
    store_id varchar(32) comment '门店id',
    updated_at date,
    k varchar(10) comment '数据名',
    v smallint comment '数据值',
    user_type tinyint comment '用户类型(0:全部1:会员 2:员工 3惯偷 4:回头客5:普通顾客)',
    t varchar(10) comment '数据类型',
    primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
