require 'openssl'
require 'base64'
require 'digest'

class AlipaySign
    def self.sign(h)
        # 排序
        keys = h.keys.sort
        # 拼接
        s = ''
        0.upto(keys.size-1) do |i|
            s << '&' if i > 0
            s << "#{keys[i]}=#{h[keys[i]]}"
        end
        # 签名
        rsa_private = $container['rsa_private']
        digest = OpenSSL::Digest::SHA256.new
        pkey = OpenSSL::PKey::RSA.new(File.read(File.expand_path("../../../#{rsa_private}", __FILE__)))
        signature = pkey.sign(digest, s)
        Base64.encode64(signature)
    end
end