class OrderHelper
    def self.check_order(input)
        # 一药订单有关的字段
        return [400, 'DEPTNO 门店id必填'] if input['DEPTNO'].nil?
        return [400, 'SALEAMT 整单交易金额必填'] if input['SALEAMT'].nil?
        return [400, 'CREATETIME 交易时间必填'] if input['CREATETIME'].nil?
        return [400, 'CASHTYPE 收款类型必填'] if input['CASHTYPE'].nil?
        return [400, 'POSID 设备号必填'] if input['POSID'].nil?

        details = input['details']
        details.each do |d|
            return [400, 'DEPTNO 门店id必填'] if d['DEPTNO'].nil?
            return [400, 'ORDERDETAILID 销售明细流水号必填'] if d['ORDERDETAILID'].nil?
            return [400, 'ARTIID 商品id必填'] if d['ARTIID'].nil?
            return [400, 'SALEQTY 销售数量必填'] if d['SALEQTY'].nil?
            return [400, 'SALEPRICE 销售单价必填'] if d['SALEPRICE'].nil?
            return [400, 'SALEAMT 销售金额必填'] if d['SALEAMT'].nil?
            return [400, 'CREATETIME 交易时间必填'] if d['CREATETIME'].nil?
            return [400, 'BATCHCODE 批号必填'] if d['BATCHCODE'].nil?
            return [400, 'VALIDDATE 效期必填'] if d['VALIDDATE'].nil?
        end
        nil
    end

    def self.save_order(input, trade_no, order_no)
        member_no = ''
        member_no = input['MEMCARDNO'] if !input['MEMCARDNO'].nil?
        details = input['details']
        BlAccount.transaction do 
            BlAccount.create!(deptno: input['DEPTNO'], orderid: order_no, saleamt: input['SALEAMT'], createtime: Time.at(input['CREATETIME']/1000), cashtype: input['CASHTYPE'], posid: input['POSID'], memcardno: member_no, tranid: trade_no)
            details.each do |d|
                BlOrderInfo.create!(deptno: d['DEPTNO'], orderid: order_no, orderdetailid: d['ORDERDETAILID'], artiid: d['ARTIID'], saleqty: d['SALEQTY'], saleprice: d['SALEPRICE'], saleamt: d['SALEAMT'], createtime: Time.at(d['CREATETIME']/1000), status: 0, batchcode: d['BATCHCODE'], validdate: d['VALIDDATE'])
            end
        end
    end
end

