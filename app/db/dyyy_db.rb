class DyyyDb < ActiveRecord::Base
    self.abstract_class = true
    self.pluralize_table_names = false
    db = YAML::load(IO.read(File.expand_path('../../../db/config.yml', __FILE__)))
    establish_connection(db[ENV['RACK_ENV']])
end