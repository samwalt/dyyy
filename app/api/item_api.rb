
# 药品分类接口
get '/category' do 
    $logger.info("/category params: #{params.to_s}")
    return [400, 'store_id 门店id必填'] if params['store_id'].nil?
    deptno = params['store_id']
    categories = BlArtistk.where('deptno = ? and ybarti = 1', deptno).distinct.pluck('articlasscode').select {|c| !c.nil?}.map {|c| c.encode!('utf-8')}
    [200, categories.to_json]
end

# 商品接口
get '/item' do
    $logger.info("/item params: #{params.to_s}")
    return [400, 'store_id 门店id必填'] if params['store_id'].nil?
    store_id = params['store_id']
    item = nil
    if !params['id'].nil?
        # 药品目录用
        id = params['id'].to_i
        item = BlArtistk.find_by(artiid: id, deptno: store_id, ybarti: 1)
        h = set_image(item, '详情图', $container['image_server']) if item
        json = JSON.generate(h)
    elsif !params['drug_name'].nil?
        # 药品目录用
        drug_name = params['drug_name']
        items = BlArtistk.select('ARTINAME, retailprice, artiid').where('ARTINAME like ? and deptno = ? and ybarti = 1', "%#{drug_name}%", store_id)
        a = enrich_images(items)
        json = JSON.generate(a)
    elsif !params['category'].nil?
        # 药品目录用
        category = params['category']
        items = BlArtistk.select('ARTINAME, retailprice, artiid').where('articlasscode = ? and deptno = ? and ybarti = 1', category.encode('gb2312'), store_id)
        a = enrich_images(items)
        json = JSON.generate(a)
    else
        # 药品目录用
        items = BlArtistk.select('ARTINAME, retailprice, artiid').where(deptno: store_id, ybarti: 1)
        a = enrich_images(items)
        json = JSON.generate(a)
    end
    [200, json]
end

def enrich_images(items)
    a = []
    image_server = $container['image_server']
    items.each do |item|
        h = set_image(item, '列表图', image_server)
        a << h
    end
    a
end

def set_image(item, image_type, image_server)
    images = BlDrugImage.where(artiid: item.artiid, type: image_type).pluck('path').map {|i| i.insert(0, image_server)}
    h = JSON.parse(item.to_json)
    h['images'] = images
    h
end