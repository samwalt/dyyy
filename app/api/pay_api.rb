
APP_ID = $container['app_id']
URL = 'https://openapi.alipay.com/gateway.do'

# 刷脸支付初始化
post '/alipay/init' do
    input = JSON.parse(request.body.read)
    $logger.info("/alipay/init params: #{input.to_s}")
    h = {}
    h['biz_content'] = input['biz_content']
    h['app_id'] = APP_ID
    h['method'] = 'zoloz.authentication.customer.smilepay.initialize'
    h['charset'] = 'utf-8'
    h['sign_type'] = 'RSA2'
    h['version'] = '1.0'
    h['timestamp'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')
    sign = AlipaySign.sign(h)
    h['sign'] = sign

    body = HTTP.post(URL, :params => h).body
    [200, body]
end

# 刷脸支付和收單
post '/alipay/pay' do
    input = JSON.parse(request.body.read)
    $logger.info("/alipay/pay params: #{input.to_s}")

    a = OrderHelper.check_order(input)
    return a if !a.nil?

    h = {}
    # 公共参数
    h['app_id'] = APP_ID
    h['method'] = 'alipay.trade.pay'
    h['charset'] = 'utf-8'
    h['sign_type'] = 'RSA2'
    h['version'] = '1.0'
    h['timestamp'] = Time.now.strftime('%Y-%m-%d %H:%M:%S')

    # 订单号，用时间表示
    order_no = Time.now.strftime('%Y%m%d%H%M%S%L')
    biz_content = {}
    biz_content['auth_code'] = input['auth_code']
    biz_content['total_amount'] = input['SALEAMT']
    biz_content['scene'] = 'security_code'
    biz_content['out_trade_no'] = order_no
    biz_content['subject'] = '第一医药刷脸支付'
    h['biz_content'] = JSON.generate(biz_content)

    sign = AlipaySign.sign(h)
    h['sign'] = sign
    $logger.info("传给支付宝的参数#{h.to_json}")
    body = HTTP.post(URL, :params => h).body
    response = JSON.parse(body)
    code = response['alipay_trade_pay_response']['code']
    if !'10000'.eql?(code)
        $logger.info("调支付宝收单出错，code: #{code}, msg: #{response['alipay_trade_pay_response']['msg']}, sub_code: #{response['alipay_trade_pay_response']['sub_code']}, sub_msg: #{response['alipay_trade_pay_response']['sub_msg']}")
        return [500, "调支付宝收单出错，code: #{code}, msg: #{response['alipay_trade_pay_response']['msg']}, sub_code: #{response['alipay_trade_pay_response']['sub_code']}, sub_msg: #{response['alipay_trade_pay_response']['sub_msg']}"]
    end

    trade_no = response['alipay_trade_pay_response']['trade_no']
    OrderHelper.save_order(input, trade_no, order_no)
    result = {}
    result['ORDERID'] = order_no
    result['TranID'] = trade_no
    [200, JSON.generate(result)]
end