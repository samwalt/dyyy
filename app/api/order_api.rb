
# 仅收单
post '/order' do 
    input = JSON.parse(request.body.read)
    $logger.info("/order params: #{input.to_s}")

    return [400, 'TranID 支付订单号必填'] if input['TranID'].nil?
    a = OrderHelper.check_order(input)
    return a if !a.nil?

    deptno = input['DEPTNO']
    order_no = deptno + Time.now.strftime('%Y%m%d%H%M%S%L')
    trade_no = input['TranID']
    OrderHelper.save_order(input, trade_no, order_no)
    [200, "{\"orderid\": #{order_no}}"]
end


