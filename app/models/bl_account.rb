class BlAccount < DyyyDb
    validates :deptno, presence: {message: '门店ID必填'}
    validates :orderid, presence: {message: '销售单号必填'}
    validates :saleamt, presence: {message: '整单交易金额必填'}
    validates :createtime, presence: {message: '交易时间必填'}
    validates :cashtype, presence: {message: '收款类型必填'}
    validates :posid, presence: {message: '设备号必填'}
end