class BlOrderInfo < DyyyDb
    validates :deptno, presence: {message: '门店ID必填'}
    validates :orderid, presence: {message: '销售单号必填'}
    validates :orderdetailid, presence: {message: '销售明细流水号必填'}
    validates :artiid, presence: {message: '商品ID必填'}
    validates :saleqty, presence: {message: '销售数量必填'}
    validates :saleprice, presence: {message: '销售单价必填'}
    validates :saleamt, presence: {message: '销售金额必填'}
    validates :createtime, presence: {message: '交易时间必填'}
    validates :status, presence: {message: '交易状态必填'}
end