require 'base64'
require 'openssl'
require 'digest'

PRIVATE_KEY = File.read('id_rsa_test')
PUBLIC_KEY = File.read('id_rsa_test.pub')

data = 'Sign me!'
digest = OpenSSL::Digest::SHA256.new
pkey = OpenSSL::PKey::RSA.new(PRIVATE_KEY)
signature = Base64.encode64(pkey.sign(digest, data))

puts signature

pub = OpenSSL::PKey::RSA.new(PUBLIC_KEY)
puts pub.verify(digest, Base64.decode64(signature), data)